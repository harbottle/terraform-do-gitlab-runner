variable "region" {}

resource "digitalocean_droplet" "gitlab-runner" {
  image = "centos-7-x64"
  name = "gitlab-runner"
  region = "${var.region}"
  size = "1gb"
  private_networking = false
  ssh_keys = [
    "${var.ssh_fingerprint}"
  ]
  connection {
    user = "root"
    type = "ssh"
    private_key = "${file(var.pvt_key)}"
    timeout = "2m"
    host = "${digitalocean_droplet.gitlab-runner.ipv4_address}"
  }
  provisioner "file" {
    source      = "install.sh"
    destination = "/tmp/install.sh"
  }
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/install.sh",
      "/tmp/install.sh",
    ]
  }
}
