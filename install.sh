#!/bin/bash

# idempotent repo function
irepo() {
  ipkg yum-utils
  ipkg wget
  if [[ ! -z "${2}" ]]; then
    if [[ ! -e "/etc/yum.repos.d/${2}" ]]; then
      echo "Repo: ${2}: installing"
      wget -q -O "/etc/yum.repos.d/${2}" "${1}"
    else
       echo "Repo: ${2}: already installed"
    fi
  else
    if [[ ! -e "/etc/yum.repos.d/${1##*/}" ]]; then
      echo "Repo: ${1##*/}: installing"
      yum-config-manager --add-repo "${1}" > /dev/null
    else
       echo "Repo: ${1##*/}: already installed"
    fi
  fi
}

# idempotent package function
ipkg() {
  if ! rpm -q ${1} > /dev/null 2>&1; then
    echo "Package: ${1}: installing"
    yum -y -q install ${1}
  else
    echo "Package: ${1}: already installed"
  fi
}

# idempotent group function
igroup() {
  if ! getent group ${1} > /dev/null 2>&1; then
    echo "Group ${1}: creating"
    groupadd ${1}
  else
    echo "Group: ${1}: already created"
  fi
}

# idempotent service start function
isvc_start() {
  if ! systemctl is-active --quiet ${1}; then
    echo "Service ${1}: starting"
    systemctl start ${1}
  else
    echo "Service: ${1}: already started"
  fi
}

# idempotent service enable function
isvc_enable() {
  if ! systemctl is-enabled --quiet ${1}; then
    echo "Service ${1}: enabling"
    systemctl start ${1}
  else
    echo "Service: ${1}: already enabled"
  fi
}

# Install and configure Docker-CE
ipkg device-mapper-persistent-data
ipkg lvm2
irepo https://download.docker.com/linux/centos/docker-ce.repo
ipkg docker-ce
ipkg docker-ce-cli
ipkg containerd.io
igroup docker
isvc_start docker
isvc_enable docker

# Install and configure GitLab Runner
irepo 'https://packages.gitlab.com/install/repositories/runner/gitlab-runner/config_file.repo?os=el&dist=7' 'runner_gitlab-runner.repo'
ipkg gitlab-runner
isvc_start gitlab-runner
isvc_enable gitlab-runner
